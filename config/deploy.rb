set :stages, ["testing", "staging", "production"]
set :default_stage, "testing"
# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'commpack_database_migration'

set :repo_url, 'git@bitbucket.org:solvup/commpack-database-migrations.git'

set :tmp_dir, '/tmp/temp_cap'
set :git_https_username, 'solvup-dev'

set :deployment_root_directory, '/var/www/commpack-database-migration' #todo: change to /var/www/xxxx, same as the deploy_to in deploy/testing.rb

set :deployment_current_directory, "#{fetch(:deployment_root_directory)}/current"
# currently only keep var/logs in shared folder. Should we keep the whole "var" folder? In Symfony3 "var" folder contains 3 sub-dirs: logs, cache, sessions.
set :deployment_shared_directory, "#{fetch(:deployment_root_directory)}/shared"

set :permission_method, :chmod
set :file_permissions_users, ["ubuntu"]
#set :file_permissions_paths, ["var/cache"]
set :use_composer, false
set :composer_install_flags, '--no-dev'

#Slack
set :slack_webhook, "https://hooks.slack.com/services/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"
set :slack_channel, '#dt-deployment'

#set :app_config_path, 'app/config'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
set :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5


namespace :deploy do

  desc "setting permission"
    task :setting_permission do
     on roles(:all) do
      #execute "chown -R ubuntu:ubuntu #{fetch(:deployment_current_directory)}"
    end
  end

  desc "run migration script"
    task :run_migration do
      on roles(:all) do
       execute "cd '#{release_path}'; pwd; php vendor/bin/phinx migrate -c phinx.yml -e #{fetch(:phinx_environment)}" 
    end
  end


  after :updating, :setting_permission
  after :finished, :run_migration

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

